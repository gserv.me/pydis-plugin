package me.gserv.pydis.listeners

import me.gserv.pydis.Plugin
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerPreLoginEvent

class JoinListener(private val plugin: Plugin) : Listener {

    @EventHandler fun onPlayerJoin(event: AsyncPlayerPreLoginEvent) {
        val result = this.plugin.mcLeaksAPI.checkAccount(event.uniqueId)

        if (result.hasError()) {
            this.plugin.logger.severe("Failed to check if ${event.name} is using MCLeaks: ${result.error}")
        } else if (result.isMCLeaks) {
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "We do not support MCLeaks accounts")
        }
    }
}
