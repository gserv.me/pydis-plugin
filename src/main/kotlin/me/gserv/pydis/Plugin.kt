package me.gserv.pydis

import me.gong.mcleaks.MCLeaksAPI
import me.gserv.pydis.listeners.JoinListener
import org.bukkit.plugin.java.JavaPlugin

class Plugin : JavaPlugin() {
    val mcLeaksAPI: MCLeaksAPI = MCLeaksAPI.builder().build()

    override fun onEnable() {
        super.onEnable()

        this.server.pluginManager.registerEvents(JoinListener(this), this)
    }

    override fun onDisable() {
        super.onDisable()
    }
}
