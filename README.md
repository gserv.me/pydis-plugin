# PyDis

**PyDis** is a custom plugin for a server I host for [Python Discord](https://pythondiscord.com). It's not intended
to be used on other server, but if you feel like it'd be useful, go ahead!

---

## Project Status

This project is still in the early stages, and it's not ready to be used just yet.

## Requirements

* [KotlinPlugin](https://www.spigotmc.org/resources/kotlinplugin-allow-to-use-kotlin-corountines-in-your-plugins.70526/)

## Building the project

This project makes use of **Gradle**. Open a terminal and run the following commands from the project directory:

* **Windows**: `gradlew jar`
* **Linux/Mac**: `./gradlew jar`

You will require a JDK installed for this - version 8 or later.

Once this task has been run, you will find a JAR file in `build/lib`, named `PyDis-<version>.jar`. This is the
plugin - drop it in your `plugins/` folder to get started.

## Questions & Contributions

Questions, concerns, ideas? [Open an issue!](https://gitlab.com/gserv.me/MCord/issues/new) Pull requests are
welcome, once the project has gotten off the ground.
